import unittest
import ftpClient


class TestCalc(unittest.TestCase):
    def test_get_list_file_if_item_exist(self):
        result = ftpClient.get_file_list("E:\\spark-2.4.4-bin-hadoop2.7")
        print(result)
        self.assertListEqual(result, ["spark-2.4.4-bin-hadoop2.7.tar"])

    def test_get_list_file_if_empty(self):
        result = ftpClient.get_file_list("E:\\Temp")
        print(result)
        self.assertTrue(not result)


if __name__ == '__main__':
    unittest.main()
