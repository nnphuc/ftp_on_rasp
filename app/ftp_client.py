import os
import sys
from ftplib import FTP
import const

from loguru import logger


def upload_ftp(host, port, user, pwd, directory_path):
    ftp = FTP()
    ftp.connect(host, port)
    ftp.login(user, pwd)

    place_files(ftp, directory_path)

    ftp.quit()


def place_files(ftp, path):
    for name in os.listdir(path):
        localpath = os.path.join(path, name)
        if os.path.isfile(localpath):
            logger.info("STOR", name, localpath)
            try:
                ftp.storbinary(
                    'STOR ' + name, open(localpath, 'rb')
                )  # Upload file
                os.remove(localpath)  # Delete file
            except:
                logger.error(sys.exc_info()[0])
        else:
            logger.error("Directory", localpath)
        # elif os.path.isdir(localpath):
        #     logger.info("MKD", name)

        #     try:
        #         ftp.mkd(name)

        #     # ignore "directory already exists"
        #     except error_perm as err:
        #         if not err.args[0].startswith('550'):
        #             raise

        #     logger.info("CWD", name)
        #     ftp.cwd(name)
        #     place_files(ftp, localpath)
        #     logger.info("CWD", "..")
        #     ftp.cwd("..")


def job():
    for name in const.FTP_DIR:
        upload_ftp(
            const.FTP_HOST,
            const.FTP_PORT,
            const.FTP_USER,
            const.FTP_PWD,
            name,
        )
