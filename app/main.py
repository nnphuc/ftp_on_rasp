import time
import schedule
from ftp_client import job

# from logger_wrapper import config, create_log

# config()

if __name__ == "__main__":
    schedule.every().hour.do(job)
    while True:
        schedule.run_pending()
        time.sleep(1)
