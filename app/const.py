import os

DEFAULT_LOG_PATH = os.path.join(os.getcwd(), "logs")

FTP_HOST = os.getenv('FTP_HOST')
FTP_PORT = os.getenv('FTP_PORT')
FTP_USER = os.getenv('FTP_USER')
FTP_PWD = os.getenv('FTP_PWD')
FTP_DIR = os.getenv('FTP_DIR')
